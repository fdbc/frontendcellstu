# Imagen base
FROM node:latest

# Directorio de la app
WORKDIR /app

# Copio archivos
ADD build/default /app/build/default
ADD package.json /app
ADD server.js /app
ADD css /app
ADD fonts /app
ADD images /app

# Dependencias
#RUN npm install

# Puerto --> el de la aplicación (npm run start)
EXPOSE 3000

# Comando
CMD ["npm", "start"]
