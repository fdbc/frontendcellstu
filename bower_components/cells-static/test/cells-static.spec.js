import CellsStatic from '../src/index';
import './mocks/rewire';

import { HOOKS } from '../src/router/constants';
const { ROUTER, ROUTE } = HOOKS;

const createSpys = (spyNames) => Object.assign({}, ...Object.keys(spyNames).map(key => ({[key]: sinon.spy()})));

const routerSpys = createSpys(ROUTER);
const routeSpys = createSpys(ROUTE);

const hooks = {
  onStart: function() {
    routeSpys.onStart();
  },
  onActivate: function(toState, fromState) {
    routeSpys.onActivate();

    return Promise.resolve({});
  },
  onResolve: function(toState, fromState) {
    routeSpys.onResolve();

    return Promise.resolve({});
  },
  onCancel: function() {
    routeSpys.onCancel();
  },
  onError: function() {
    routeSpys.onError();
  },
  onSuccess: function() {
    routeSpys.onSuccess();
  },
};

const config = {
  pageRender: {
    mainNode: '#app__container',
    componentPath: 'base/test/mocks/app/pages',
    progressive: true,
    maxSimultaneousPages: 6
  },
  router: {
    routes: [
      { name: 'dashboard', path: '/', ...hooks }
    ],
    defaultRoute: '/',
    hooks: {
      onTransitionStart: function(toState, fromState) {
        routerSpys.onTransitionStart();
      },
      onTransitionActivate: function(toState, fromState) {
        routerSpys.onTransitionActivate();

        return Promise.resolve({});
       },
      onTransitionResolve: function(toState, fromState) {
        routerSpys.onTransitionResolve();

        return Promise.resolve({});
      },
      onTransitionCancel: function(toState, fromState) {
        routerSpys.onTransitionCancel();
      },
      onTransitionError: function(toState, fromState, err) {
        routerSpys.onTransitionError();
      },
      onTransitionSuccess: function(toState, fromState) {
        routerSpys.onTransitionSuccess();
      }
    }
  }
};

const resetSpys = (...args) => {
  args.forEach(item => {
    Object.keys(item).forEach(key => {
      item[key].reset();
    });
  });
};

describe('Cells Static', () => {
  beforeEach(() => {
    resetSpys(routerSpys, routeSpys);

    CellsStatic.config(config);
  });

  describe('router hooks', () => {
    it('should trigger router general hooks if they are defined', (done) => {
      setTimeout(function() {
        const calledOnce = ['onTransitionStart', 'onTransitionActivate', 'onTransitionResolve', 'onTransitionSuccess'];
        const notCalled = ['onTransitionCancel', 'onTransitionError'];

        calledOnce.forEach(prop => routerSpys[prop].should.have.been.calledOnce);
        notCalled.forEach(prop => routerSpys[prop].should.not.have.been.called);
        done();
      }, 1000);
    });

    it('should trigger error hook if route is not defined', (done) => {
      CellsStatic.Router.navigate('no-existe');

      setTimeout(function() {
        routerSpys.onTransitionError.should.have.been.calledOnce;
        done();
      }, 1000);
    });
  });

  describe('route hooks', () => {
    it('should trigger route hooks if they are defined', (done) => {
      setTimeout(function() {
        const calledOnce = ['onStart', 'onActivate', 'onResolve', 'onSuccess'];

        calledOnce.forEach(prop => routeSpys[prop].should.have.been.calledOnce);
        done();
      }, 1000);
    });
  });
});
