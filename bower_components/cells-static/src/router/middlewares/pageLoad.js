import Utils from '../utils';
import { importLazyElement } from '../lib/importLazyElement';

const { normalizePageName, getPageComponentName, getPageComponentPath, isCustomElementRegistered } = Utils;

const pageLoad = (componentPath) => (router) => (toState, fromState) => {
  const stateName = toState.name;
  const normalizedPageName = normalizePageName(stateName);
  const pageComponentName = getPageComponentName(normalizedPageName);
  const pageComponentPath = getPageComponentPath(componentPath, normalizedPageName);

  console.log('pageLoadMiddleware');
  console.log('checking register for component...' + pageComponentName);

  if (isCustomElementRegistered(pageComponentName)) {
    return true;
  }

  return importLazyElement(pageComponentPath).then(() => true).catch(() => new Error());
};

export default pageLoad;
