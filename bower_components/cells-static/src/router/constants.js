const ROUTER_HOOKS = {
  onTransitionActivate: 'onTransitionActivate',
  onTransitionStart: 'onTransitionStart',
  onTransitionResolve: 'onTransitionResolve',
  onTransitionCancel: 'onTransitionCancel',
  onTransitionError: 'onTransitionError',
  onTransitionSuccess: 'onTransitionSuccess',
  onTransitionExit: 'onTransitionExit'
};

const ROUTE_HOOKS = {
  onActivate: 'onActivate',
  onStart: 'onStart',
  onResolve: 'onResolve',
  onCancel: 'onCancel',
  onError: 'onError',
  onSuccess: 'onSuccess',
  onExit: 'onExit'
};

export const HOOKS = {
  ROUTER: ROUTER_HOOKS,
  ROUTE: ROUTE_HOOKS
};

export const ROUTER5_ROUTE_HOOK_INTERCEPTOR =  'canActivate'; // Native Router5 route hook. Used to extend with custom ones (they are injected instead of onActivate so Router5 engine will work as expected)

export default {
  HOOKS,
  ROUTER5_ROUTE_HOOK_INTERCEPTOR
};
