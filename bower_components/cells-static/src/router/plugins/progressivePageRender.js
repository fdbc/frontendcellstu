import CoreUtils from '../../core/utils';
import RouterUtils from '../utils';
import { querySelector } from '../lib/shadowQuerySelector';

const { pipe } = CoreUtils;
const { normalizePageName, getPageComponentName } = RouterUtils;

const normalizedPageComponentName = pipe(normalizePageName, getPageComponentName);

const _createPage = (name) => ({ name, element: document.createElement(name) });
const _findPage = (name, pages) => pages.find(p => p.name === name);

const progressivePageRender = (maxSimultaneousPages) => (mainNode) => () => {
  const pages = [];
  let appNode = null;

  return {
    onTransitionSuccess(toState, fromState) {
      const { params } = toState;
      const fromPageComponentName = fromState ? normalizedPageComponentName(fromState.name) : null;
      const toPageComponentName = normalizedPageComponentName(toState.name);

      console.log('pageRender::onTransitionSuccess()');

      if (!appNode) {
        appNode = querySelector(mainNode, document.body);

        if (!appNode) {
          console.error(`${mainNode} node not found - ${toPageComponentName} could not be rendered.`);
          return;
        }
      }

      const fromPage = _findPage(fromPageComponentName, pages);
      let toPage = _findPage(toPageComponentName, pages);
      const shouldCreateToPage = !toPage;

      if (shouldCreateToPage) {
        toPage = _createPage(toPageComponentName);

        pages.push(toPage);

        if (pages.length > maxSimultaneousPages) {
          const pageToRemove = pages.shift();

          appNode.removeChild(pageToRemove.element);
        }
      } else {
        toPage.element.removeAttribute('hidden');
      }

      const shouldBindParams = toPage.element.params && Object.keys(params).length > 0;

      if (shouldBindParams) {
        toPage.element.params= params;
      }

      if (shouldCreateToPage) {
        appNode.appendChild(toPage.element);
      }

      if (fromPage) {
        fromPage.element.setAttribute('hidden', true);
      }
    }
  }
};

export default progressivePageRender;
