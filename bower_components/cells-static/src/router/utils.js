import dasherize from 'dasherize';

const resolveFileLocation = (file) => window.location.protocol + '//' + window.location.hostname + (window.location.port?":"+window.location.port:"") + window.location.pathname.replace(/[^/]*$/, '') + file;

export const normalizePageName = pageName => dasherize(pageName).replace(/\./g,'-');

export const getPageComponentName = pageName => `${pageName}-page`;

export const getPageComponentPath = (componentPath, pageComponentName) => resolveFileLocation(`${componentPath}/${pageComponentName}.html`);

export const isCustomElementRegistered = name => customElements.get(name) !== undefined;

export default {
  normalizePageName,
  getPageComponentName,
  getPageComponentPath,
  isCustomElementRegistered
};
