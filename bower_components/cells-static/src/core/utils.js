export const extendObject = (defaultConfig, config) => {
  const result = {...defaultConfig};

  for (let property in config) {
    if (result.hasOwnProperty(property)) {
      result[property] = config[property];
    }
  }

  return result;
}

export const pipe = (...fns) => x => fns.reduce((v, f) => f(v), x);

export const pipePromises = (fns) => (...args) => fns.reduce((p, fn) => p.then(fn.bind(null, ...args)), Promise.resolve());

export default {
  extendObject,
  pipe,
  pipePromises
};
