
export const callbackHandler = (cb, cbCtx) => ({
  apply(target, ctx, args) {
    Reflect.apply(target, ctx, args);
    Reflect.apply(cb, cbCtx, [ ctx ]);
  }
});


export const registerHandler = (targetKey, targetObj, handler) => {
  targetObj[targetKey] = new Proxy(targetObj[targetKey], handler);

  return targetObj;
};
