import { ReplaySubject } from 'rxjs/ReplaySubject';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/fromEvent';

export class ChannelBase {

  constructor() {
    this.__channel = new ReplaySubject(1);
  }

  subscribe(listener) {
    if (!listener) {
      console.warn('No listener provided for subscription. Subscription cancelled.');
      return false;
    }
    return this.__channel.subscribe(listener);
  }

  bindEvent(node, eventName) {
    return this._subscribeFromEvent(node, eventName);
  }

  bindProperty(node, propertyName) {
    const propertyChangeEvent = !propertyName.includes('-changed') ? `${propertyName}-changed` : propertyName;

    return this._subscribeFromEvent(node, propertyChangeEvent);
  }

  _subscribeFromEvent(node, eventName) {
    const source = Observable.fromEvent(node, eventName);

    return source.subscribe(event => this.publish(event));
  }

  publish(data) {
    this.__channel.next(data);
  }

  dispose() {
    this.__channel.unsubscribe();
  }
}
