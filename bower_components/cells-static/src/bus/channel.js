import { ChannelBase } from './channelBase';
import { updateSubscriptions } from './subscriptionsHelpers';
import { callbackHandler, registerHandler } from './proxyHelpers';


export class Channel extends ChannelBase {

  constructor(name) {
    super();
    this.__name = name;
    this.__subscriptions = [];
  }

  get name() {
    return this.__name;
  }

  subscribe(listener, options = {}) {
    const rawSubscription = super.subscribe(listener);
    const unsubscribeHandler = callbackHandler(this.__unsubscribe, this);
    const proxiedSubscription = registerHandler('unsubscribe', rawSubscription, unsubscribeHandler);
    const subscription = Object.assign({}, { value: proxiedSubscription }, options);

    this.__subscriptions.push(subscription);

    return proxiedSubscription;
  }

  subscribeOnce(listener) {
    return this.subscribeTimes(listener, 1);
  }

  subscribeTimes(listener, n) {
    return this.subscribe(listener, { times: n });
  }

  publish(data) {
    super.publish(data);
    this.__subscriptions = updateSubscriptions(this.__subscriptions);

    return this;
  }

  dispose() {
    this.__subscriptions.forEach(subscription => subscription.value.unsubscribe());
    this.__subscriptions = [];
    super.dispose();
  }

  __unsubscribe(subscription) {
    this.__subscriptions = this.__subscriptions.filter(subsc => subsc.value !== subscription);
  }

}


export class AutoDisposableChannel extends Channel {

  __unsubscribe(subscription) {
    this.__subscriptions = this.__subscriptions.filter(subsc => subsc.value !== subscription);
    if (this.__subscriptions.length === 0) {
      this.dispose();
    }
  }

}