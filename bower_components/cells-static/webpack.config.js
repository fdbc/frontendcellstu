const path = require('path');
const webpack = require('webpack');
var BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;

const LIBRARY = 'Cells';

const getFileName = deploy => {
  const fileExtension = deploy ? 'min.js' : 'js';

  return `cells-static.${fileExtension}`;
};

const webpackConfig = env => {
  const isDeploy = env && env.deploy !== undefined && env.deploy == 'true';
  const config = {
    target: 'web',
    entry: path.resolve('src/index.js'),
    output: {
      path: path.join(__dirname, '/dist'),
      filename: getFileName(isDeploy),
      library: LIBRARY,
      libraryTarget: 'umd'
    },
    module: {
      rules: [
        {
          test: /\.js$/,
          use: {
            loader: 'babel-loader'
          },
          exclude: [
            path.resolve(__dirname, "node_modules")
          ]
        }
      ]
    },
    plugins: [new BundleAnalyzerPlugin({analyzerMode: 'static'})],
    devtool: "source-map" // enum
  };

  if (isDeploy) {
    if (!config.plugins) {
      config.plugins = [];
    }

    config.plugins.push(
      new webpack.optimize.UglifyJsPlugin({
        compress: {
          drop_console: true,
          warnings: true
        }
      })
    );
  };

  return config;
};

module.exports = webpackConfig;
