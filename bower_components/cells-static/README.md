# cells-static

Cells Static is a lightweight library that provides an easy solution for building modular, maintainable, & high scalable Progressive Web Apps based on Polymer 2 using existing tooling for build, deploy, PRPL...

You only need build Web Components for building reusable UI elements with [Polymer](https://www.polymer-project.org/2.0/docs/devguide/feature-overview) and Cells for structuring and orchestrating the application. 


Additionally Cells provides a [catalog](http://bbva-files.s3.amazonaws.com/cells/bbva-catalog/index.html#/?v=1.7.0)  that you can use to build your application

## <a name="appendix"></a>Appendix

0. [Requirements](#requirements)
1. [Instructions](#instructions)
2. [Configuration](#configuration)
	- [pageRender](#pageRender)
		- [mainNode](#mainNode)
		- [componentPath](#componentPath)
		- [progressive](#progressive)
		- [maxSimultaneousPages](#maxSimultaneousPages)
	- [router](#routerConfiguration)
		- [useHash](#useHash)	 
	 	- [routes](#routes)
	 	- [defaultRoute](#defaultRoute)
	 	- [hooks](#hooks)
3. [Router](#router)
	- [General Router Lifecycle Hooks](#general-router-lifecycle-hooks)
	- [Individual Route Lifecycle Hooks](#individual-route-lifecycle-hooks)
	- [Mapping route parameters to page component properties](#mapping-route-parameters-to-page-component-properties)
	- [Example of custom redirection (not default route) on transition reject](#example-custom-redirection-transition-reject)
	- [API](#router-api)
4. [Communication Bus (Pub/Sub)](#communication-bus)
	- [Bus API](#bus-api)
	- [Channel API](#channel-api)
	- [Example](#bus-example)
5. [Suggested application architecture](#suggested-application-architecture)
6. [Sample app](#sample-app)

## <a name="requirements"></a>0. Requirements

To create an application template and install Polymer automatically, you can use the Polymer CLI.

Install the Polymer CLI.

```no-highlight
npm install -g polymer-cli
```

For full installation instructions, [see the Polymer CLI documentation](https://www.polymer-project.org/2.0/start/install-2-0)

Compete list of [commands](https://www.polymer-project.org/2.0/docs/tools/polymer-cli-commands) 


**[⬆️ back to top](#appendix)**

---

## <a name="instructions"></a>1. Instructions

To install

```no-highlight
bower install --save ssh://globaldevtools.bbva.com:7999/CEL/cells.git
```

To use

```html
<!-- Load cells-static.min.js inside your index.html -->
<script src="bower_components/webcomponentsjs/webcomponents-loader.js"></script>
<script src="bower_components/cells-static/dist/cells-static.min.js"></script>
```

**[⬆️ back to top](#appendix)**

---

## <a name="configuration"></a>2. Configuration

To configure and start Cells Static, you only need to call `config` method of Cells Static library passing the desired configuration object.

Here is an example of a basic Cells Static application configuration:

```javascript
// Cells configuration inside app.js, loaded by your application shell component.
const hasPermissions = state => true; // Custom logic to check for acces....

// onActivate route hook implementation.
const _onActivate = (toState, fromState, done) => {
	// For example, we can check if user has access to current page...
	if (!hasPermissions(toState)) {
		return Promise.reject({description: 'You are trying to access a private area.'});
	}

	return Promise.resolve();
};

// Retrieval of remote data...
const _loadProducts = () => return new Promise((resolve, reject) => resolve([{...}]);

// onResolve route hook implementation.
const _onResolve = (toState, fromState, done) => {
	// For example, we can use onResolve hook to make some data available on the Bus before route has been successfully loaded and rendered to the user...
	return _loadProducts().then(products => {
		Cells.Bus.channel('products').publish(products);
		return Promise.resolve();
	});
};

Cells.config({
	pageRender: {
		mainNode: '#app__container',
		componentPath: 'app/pages',
		progressive: true,
		maxSimultaneousPages: 6
	},
	router: {
		useHash: true,
		routes: [
    	{ name: 'dashboard', path: '/', onActivate: _onActivate },
			{ name: 'dashboard.products', path: '/products', onResolve: _onResolve }
		],
		defaultRoute: 'dashboard',
		hooks: {}
	}
});
```

The following properties are allowed to be defined on Cells configuration object:

| Name | Description | Value | Example |
|:---|:---|:---|:---:|
| pageRender | Object containing information regarding page render configuration (where components are going to be rendered, component path, render mode...) | `Object`  | [pageRender](#pageRender) |
| router | Object containing information regarding router configuration (routes, deault route, hooks...) | `Object` | [Hooks](#router) |

`* Mandatory properties.`

**[⬆️ back to top](#appendix)**

---

### <a name="pageRender"></a>pageRender

`pageRender` property object allows you to define the configuration regarding the page render configuration.

**[⬆️ back to top](#appendix)**

---

#### <a name="mainNode"></a>`mainNode`

`mainNode` subproperty allows to specify the CSS selector where page components (routable components) are going to be rendered dynamically.

_You are allowed to use CSS selectors that refers to HTML elements hosted inside Shadow DOM, but keep in mind that performance may decrease._

Example:

```javascript
// Cells configuration inside app.js, loaded by your application shell component.
Cells.config({
	pageRender: {
		mainNode: '#app__container'
	},
  ...
});
```

```html
// Application Shell.
<link rel="import" href="../bower_components/polymer/polymer-element.html">

<script src="./app.js"></script>

<dom-module id="app-shell">
  <template>
    <style></style>
    <!-- shell components: header, side-bar, footer, etc... -->

    <!-- Page components are going to be rendered inside this div, that matchs mainNode value (#app__container) -->
    <div id="app__container"></div>
  </template>

  <script>
    class AppShell extends Polymer.Element {
      static get is() {
        return 'app-shell';
      }
    }

    window.customElements.define(AppShell.is, AppShell);
  </script>
</dom-module>
```

**[⬆️ back to top](#appendix)**

---

#### <a name="componentPath"></a>`componentPath`

`componentPath` subproperty allows to define the base path where page components (routable components) are going to be gathered.

_Default value is set to `app/pages`_

**[⬆️ back to top](#appendix)**

---

#### <a name="progressive"></a>`progressive`

`progressive` subproperty allows to enable/disable progressive page render, allowing to saved visited pages on HTML DOM, improving render-time for further visits.

_Default value is set to `true`_

---

#### <a name="maxSimultaneousPages"></a>`maxSimultaneousPages`

`maxSimultaneousPages` subproperty allows to define the max page number to be saved on HTML DOM for further visits while `progressive` mode is enabled.

_Default value is set to `6`_

**[⬆️ back to top](#appendix)**

---

### <a name="routerConfiguration"></a>router

`router` property object allows you to define the configuration regarding the router (routes, default route, and router hooks).

**[⬆️ back to top](#appendix)**

---

#### <a name="useHash"></a>`useHash`

`useHash` subproperty allows you to specify the use of hash to map routes path to browser url.

_Default value is set to `true`._

**[⬆️ back to top](#appendix)**

---

#### <a name="routes"></a>`routes`

`routes` subproperty allows you to define the routes for your application. They are POJO with the following properties:

| Property    | Description                         | Value         |
|:------------|:------------------------------------|:-------------:|
| name        | Route name.                         | `String`      |
| path        | URL path for given route.           | `String`      |
| data        | Metadata associated to given route. | Object        |
| ...hooks    | Implementation of available route hooks (`onStart, onActivate, onResolve, onCancel, onError, onSuccess, onExit`) | Function  |

`* Mandatory properties.`

__Route hooks are specified as properties on route object. Neither of them are required.__

Example:

```javascript
const _onActivate = (toState, fromState, done) => {
	// For example, we can check if user has access to current page...
	if (!hasPermissions()) {
		return Promise.reject({description: 'You are trying to access a private area.'});
	}

	return Promise.resolve();
};

// Make an Ajax Call...
const _loadProducts = () => return new Promise((resolve, reject) => resolve([{...}]);

const _onResolve = (toState, fromState, done) => {
	// For example, we can use onResolve hook to make some data available on the Bus before route has been successfully loaded and rendered to the user...
	return _loadProducts().then(products => {
		Cells.Bus.channel('products').publish(products);
		return Promise.resolve();
	});
};

const routes = [
	{ name: 'dashboard', path: '/', onActivate: _onActivate },
	{ name: 'dashboard.products', path: '/products', onResolve: _onResolve },
	...,
	...
];
```

_Check section [Individual Route Lifecycle Hooks](#individual-route-lifecycle-hooks) to learn more in depth about route hooks._

**[⬆️ back to top](#appendix)**

---

#### <a name="defaultRoute"></a>`defaultRoute`

`defaultRoute` subproperty allows you to specify the route name that will be used for redirecting users when they hit a non-defined path route or when there is a transition error / abort.

Example:

```javascript
Cells.config({
  mainNode: '#app__container',
  router: {
    hooks: {},
    routes: [
        { name: 'dashboard', path: '/', onActivate: _onActivate },
        { name: 'dashboard.products', path: '/products', onResolve: _onResolve }
    ],
    defaultRoute: 'dashboard'
  }
});
```

**[⬆️ back to top](#appendix)**

---

#### <a name="hooks"></a>`hooks`

`hooks` subproperty allows you to define logic that is going to be executed in different points of router lifecycle for all your routes.

It expects an object containing definition for each desired router hook to be called on Router lifecycle.

Example:

```javascript
Cells.config({
  ...,
  router: {
    hooks: {
		  onTransitionStart: function(toState, fromState) {
		    // Triggered on transition route start.
		  },
		  onTransitionActivate: function(toState, fromState) {
		    // Triggered after transition has started and before route has been resolved.
		    // _Should return promise._
		    return Promise.resolve({});
		  },
		  onTransitionResolve: function(toState, fromState) {
		    // Triggered after transition has been activated.
		    // _Should return promise._
		    return Promise.resolve({});
		  },
		  onTransitionCancel: function(toState, fromState) {
		    // Triggered on transition route cancel.
		  },
		  onTransitionError: function(toState, fromState, err) {
		    // Triggered on transition route error.
		  },
		  onTransitionSuccess: function(toState, fromState) {
		    // Triggered after transition route success.
		  },
		  onTransitionExit: function(toState, fromState) {
		    // Triggered after route has been leaved (after another router has been loaded successfully)
		  }
		},
    ...
  }
});
```

_Check section [General Router Lifecycle Hooks](#general-router-lifecycle-hooks) to learn more in depth about Router hooks_

**[⬆️ back to top](#appendix)**

## <a name="router"></a>3. Router

Cells Static library provides a routing solution based on `routable components` or `page components` that are going to be rendered dynamically inside a given HTML element defined on your application `Shell`.

Each route has their own `name` and `path`, and can be organised in a tree of routes, giving you the ability to have nested routes.

During a route transition, Router engine will:

1. Compute the component name associated for given route.

2. Check if corresponding component has been registered on CustomElements definition registry.

3. If it has been registered, Router will skip to next step. Otherwise, it will be lazy loaded & registered.

4. After component has been loaded & registered on CustomElements registry, it will be rendered dynamically inside HTML Element matching 'mainNode' selector, defined in your application Shell.

To achieve these, we must follow a few basic conventions:

1. Route name must match a component definition file on given path: `app/pages/{route-name}.html`

2. Page component definition name must match the following convention: `{route-name}-page`.

3. If it's a nested route (ex: `products.accounts`), route name will be turned into dash-case (ex: `products-accounts`) while computing component definition file path and component definition name.

Let's break it down with a real example:

Given the following routes:

```javascript
var routes = [
  {
    name: 'products',
    path: '/products'
  },
  {
    name: 'products.accounts',
    path: '/accounts'
  }
];
```

Your application folder structure must be:

```nohighlight
|---/app
|   |___/components
|   |   |____/....
|   |   |____/....
|   |   |____/....
|   |
|   |___/pages
|   |   |____/products.html (component definition name: products-page)
|   |   |____/products-accounts.hml (component definition name: products-accounts-page)
|   |
|   |___/app-shell.html
|   |___/app.js
|
|___/....
|___/polymer.json
|___/index.html
```

When the browser tries to nagivate to '/products', Router engine will do the following operations:

1. Compute component to be loaded: `products-page`.

2. Check for definition of `products-page` component inside customElements registry.

3. Already loaded? Next step. Otherwise, it will try to import component definition file from: `app/pages/products.html`

4. `products-page` will be rendered dynamically inside HTML Element matching `mainNode` selector, defined in your application `Shell`.

**[⬆️ back to top](#appendix)**

---

### <a name="general-router-lifecycle-hooks"></a>General Router Lifecycle Hooks

Cells Static router engine provides general router hooks that will be called for each route transition for all defined router lifecycle hooks. They allows you to define logic that is going to be executed in different points of router lifecycle for all your routes.

| Hook                | Description | Process type  |
|:--------------------|:------------|:------------: |
| onTransitionStart   | Triggered on transition route start. | Asynchronous  |
| onTransitionActivate    | Triggered after transition has started and before route has been resolved. | `Synchronous` |
| onTransitionResolve   | Triggered after transition has been activated. | `Synchronous` |
| onTransitionCancel  | Triggered on transition route cancel. | Asynchronous  |
| onTransitionError   | Triggered on transition route error. | Asynchronous  |
| onTransitionSuccess | Triggered after transition route success. | Asynchronous  |
| onTransitionExit | Triggered after route has been leaved (after another route has been loaded successfully). | Asynchronous  |

On each router hook call, you will receive two parameters: `toState`, and `fromState`, that will match the route you are trying to access, and the one where you are comming.

**[⬆️ back to top](#appendix)**

---

### <a name="individual-route-lifecycle-hooks"></a>Individual Route Lifecycle Hooks

Cells Static router engine also provides the capability to define individual route hooks that will be called in addition with defined general router hooks for each route.

If issued route is a nested one, and parent routes have defined hooks, they will be called from top to bottom. __If they are synchronous hooks, they will not be resolved until parent route hook has been resolved.__

| Hook        | Description | Process type  |
|:------------|:------------|:------------: |
| onStart     | Triggered on transition route start. | Asynchronous  |
| onActivate 	 | Triggered after transition has started and before route has been resolved. | `Synchronous` |
| onResolve   | Triggered after transition has been activated. | `Synchronous` |
| onCancel    | Triggered on transition route cancel. | Asynchronous  |
| onError     | Triggered on transition route error.            | Asynchronous  |
| onSuccess   | Triggered after transition route success.            | Asynchronous  |
| onExit      | Triggered after route has been leaved (after another router has been loaded successfully) | Asynchronous  |

**[⬆️ back to top](#appendix)**

---

### Example of General Router Lifecycle Hooks and Individual Route Lifecycle Hooks

```javascript
// Cells configuration inside app.js, loaded by your application shell component.
const ROUTER_HOOKS = {
  onTransitionStart: function(toState, fromState) {
    console.log('Router::Hooks::onTransitionStart()');
  },
  onTransitionActivate: function(toState, fromState) {
    console.log('Router::Hooks::onTransitionActivate()');
    return Promise.resolve({});
  },
  onTransitionResolve: function(toState, fromState) {
    console.log('Router::Hooks::onTransitionResolve()');

    return Promise.resolve({});
  },
  onTransitionCancel: function(toState, fromState) {
    console.log('Router::Hooks::onTransitionCancel()');
  },
  onTransitionError: function(toState, fromState, err) {
    console.log('Router::Hooks::onTransitionError()');
  },
  onTransitionSuccess: function(toState, fromState) {
    console.log('Router::Hooks::onTransitionSuccess()');
  },
  onTransitionExit: function(toState, fromState) {
    console.log('Router::Hooks::onTransitionExit()');
  }
};

const ROUTE_HOOKS = {
  onStart: function(toState, fromState) {
    console.log('Route::Hooks::onStart()');
  },
  onActivate: function(toState, fromState) {
    console.log('Route::Hooks::onActivate()');
    return Promise.resolve({});
  },
  onResolve: function(toState, fromState) {
    console.log('Route::Hooks::onResolve()');

    return Promise.resolve({});
  },
  onCancel: function(toState, fromState) {
    console.log('Route::Hooks::onCancel()');
  },
  onError: function(toState, fromState, err) {
    console.log('Route::Hooks::onError()');
  },
  onSuccess: function(toState, fromState) {
    console.log('Route::Hooks::onSuccess()');
  },
  onExit: function(toState, fromState) {
    console.log('Route::Hooks::onExit()');
  }
};

const ROUTES = [
  createRoute('dashboard', '/dashboard', ROUTE_HOOKS)
];

const createRoute = (name, path, hooks) => Object.assign({ name, path }, hooks);

Cells.config({
  pageRender: {
  	mainNode: '#app__container',
  },
  router: {
    hooks: ROUTER_HOOKS,
    routes: ROUTES,
    defaultRoute: ROUTES[0].name
  }
});
```

This will be the output resulting from navigation to 'dashboard' or default route:

```javascript
Router::Hooks::onTransitionStart()
Route::Hooks::onStart()
* Router::Hooks::onTransitionActivate()
* Route::Hooks::onActivate()
* Router::Hooks::onTransitionResolve()
* Route::Hooks::onResolve()
Router::Hooks::onTransitionCancel() // if transition has been cancelled...
Route::Hooks::onCancel() // if transition has been cancelled...
Router::Hooks::onTransitionError() // if there is an error while doing transition...
Route::Hooks::onError() // if there is an error while doing transition...
Router::Hooks::onTransitionSuccess()
Route::Hooks::onSuccess()
Router::Hooks::onTransitionExit() // if you are coming from a previous route...
Route::Hooks::onExit() // if you are coming from a previous route...
```

`* Synchronous hooks. Can be used to prevent access to te target route, load some data before page has ben rendered, etc...`

**[⬆️ back to top](#appendix)**

---

### <a name="mapping-route-parameters-to-page-component-properties"></a>Mapping route parameters to page component properties

Cells Static router will automatically map any parameters present on current route path to page component property `params` if it has been defined as a property on page component.

Example:

```javascript
// Somewhere where we have defined our routes...
const routes = [
	...,
	{ name: 'user-detail', path: '/user/:user-id/:user-name' },
	...,
	...
];
```

```html
// Our component definition file...
<link rel="import" href="../../bower_components/polymer/polymer-element.html">

<dom-module id="user-detail-page">
  <template>
    <style is="custom-style"></style>

    ...
  </template>

  <script>
    class UserDetailPage extends Polymer.Element {
      static get is() {
        return 'user-detail-page';
      }

      // Declare properties for the element's public API
      static get properties() {
        return {
          //IMPORTANT!!!
          params: {
            type: Object,
            value: {}
          }
        };
      }

      connectedCallback() {
        super.connectedCallback();

		//Our params property will hold the value of route parameters :user-id and :user-name.
        const userId = this.get('params.user-id');
        const userName = this.get('params.user-name');
      }
    }

    customElements.define(UsersPage.is, UsersPage);
  </script>
</dom-module>
```

**[⬆️ back to top](#appendix)**

---

### <a name="example-custom-redirection-transition-reject"></a>Example of custom redirection (not default route) on transition reject

There are some cases where you want to redirect a user to a different page than default route while you are rejecting the access of a given route.

Suppose we have the following Cells Static configuration:

```javascript
const ROUTE_HOOKS = {
  onActivate: function(toState, fromState) {
    const isAccessible = false; // We calculate if user can access to given route - it can't.

    if (!isAccessible) {
      return Promise.reject({description: 'You are trying to access a private page.', redirect: { name: 'dashboard' }})
    }

    return Promise.resolve({});
  }
};

const ROUTES = [
  createRoute('login', '/', {}),
  createRoute('dashboard', '/dashboard', {}),
  createRoute('accounts', '/accounts', ROUTE_HOOKS),
  ......
];

const createRoute = (name, path, hooks) => Object.assign({ name, path }, hooks);

Cells.config({
  pageRender: {
    mainNode: '#app__container',
  },
  router: {
    routes: ROUTES,
    defaultRoute: ROUTES[0].name
  }
});
```

Although we have defined our default route to be `login`, _we can modify the redirection route (by default, it will take default route) while we are rejecting the transition, just by adding a property `redirect` containing redirection route name to the promise rejection object while resolving the promise_.

```javascript
  onActivate: function(toState, fromState) {
	...
    return Promise.reject({description: 'You are trying to access a private page.', redirect: { name: 'dashboard' }})
  }
```

_This would not happen if you trigger manually navigation through Router API (Router.navigate). See [router API](#router-api) for more information._

**[⬆️ back to top](#appendix)**

---

### <a name="router-api"></a>API

Cells Static Router exposes a public API through the 'Router' object inside Cells global instance.

| Method | Description | Params | Return |
|:-------|:------------|:-------|:-------|
| navigate | Navigates to given route with given parameters. | `name` - Route name<br><br>`params` - Route params<br><br>`errorCallback` - Callback to be executed when navigation to given route fails (for example, route transition has been cancelled due to router hooks).<br><br>_Be aware that you need to handle manually redirection to default route (or another one) if route navigation fails - router engine will not redirect to default route as it would happen on a normal navigation._ | - |
| getCurrentRoute |	Returns current route. |	-	|	`Object`	|

**[⬆️ back to top](#appendix)**

## <a name="communication-bus"></a>4. Communication Bus (Pub/Sub)

Cells Static offers an in-memory communication bus through the exposition of 'Bus' object inside Cells global instance.

Communication bus allows to publish, broadcast, and subscribe to channel messages from anywhere across your application context (scripts, components, pages...).

It also provides an API to automatically bind component properties and component events to Channels.

**[⬆️ back to top](#appendix)**

---

### <a name="bus-api"></a>Bus API

| Method | Description | Params | Return |
|:-------|:------------|:-------|:-------|
| channel	| Returns given channel if it exists, or create it.	| `name` - Channel name (`String`) |	`Channel` |
| broadcast | Broadcast given message to all channels of the bus. | `message` - Message to be broadcasted (`Object`) | - |
| dispose | Unsubscribe all listeners from all channels. | - | - |

**[⬆️ back to top](#appendix)**

---

### <a name="channel-api"></a>Channel API

| Method | Description | Params | Return |
|:-------|:------------|:-------|:-------|
| bindEvent | Automatically publish event payload to the channel.  | `node` - Reference of target HTML Element to listen event for.<br/><br/>`eventName` - Name of the event to listen for. | `SubscriptionObject` |
| bindProperty | Automatically publish property value of target HTML Element | `node` - Reference of target HTML Element to listen event for.<br/><br/>`eventName` - Name of the event to listen for. | `SubscriptionObject` |
| subscribe | Subscribe indefinitely to channel publications. | `listener` - Callback function to be executed when a message has been received on the channel (`function`) | `SubscriptionObject` |
| subscribeOnce | Subscribe once to channel publications. | `listener` - Callback function to be executed when a message has been received on the channel (`function`) | `SubscriptionObject` |
| subscribeTimes | Subscribe given times to channel publications. | `listener` - Callback function to be executed when a message has been received on the channel (`function`)<br/><br/>`times` - Number of times the subscription will be executed (`Number`) | `SubscriptionObject` |
| publish | Publish given data to all subcriptions. | `data` - Data to be published to the subscriptions (`Object`) | `Channel`|
| dispose | Unsubscribe all channel listeners. | - | - |

**[⬆️ back to top](#appendix)**

---

### <a name="bus-example"></a>Example

Channel subscription and publish

```javascript
// We reference desired channel...
const errorChannel = Cells.Bus.channel('error');

// Channel subscription
errorChannel.subscribe(this._handleAnnouncer.bind(this));

// Channel publication
errorChannel.publish('You are trying to access a private page.');

_handleAnnouncer(announce) {
	this.set('announce', announce);
	this.$.toast.open();
}
```

Automatic binding of event and properties to channel

```javascript
// Create channel...
const eventChannel = Cells.Bus.channel('my-event-channel');
const propertyChannel = Cells.Bus.channel('my-property-channel');

// Bind to HTML Element Event...
eventChannel.bindEvent(this.$.myHTMLElement, 'target-event'); // Direct reference to myHTMLElement instance object.

// Bind to HTML Element Property...
propertyChannel.bindProperty(this.$.myHTMLElement, 'target-property'); // Direct reference to myHTMLElement instance object.

// Now, any changes made to 'target-property' or any raised event of type 'target-event', will automatically triggers a publish of the target value to the channel.
```

**[⬆️ back to top](#appendix)**

## <a name="suggested-application-architecture"></a>5. Suggested application architecture

Currently, we suggest build your single-page app (SPA) with the following structure:

* `/src`: folder with all pages and 'local' components for your app. You can rename this folder with other name, for example 'app'.
* `/src/app-shell.html` : Include the top-level app logic. It's a Polymer element that contains the base structure for your app. You can read more about shell in the section below.
* `/src/app.js`: This file contains the Cells configuration object.
* `/src/pages`: This folder contains the different pages of the app. This pages can represent the code for a particular view. Can be loaded lazily (for example parts of the app not required for first paint). The shell is responsible for dynamically importing the fragments as needed.
* `/src/components`: This folder contains local components that you can use to compose your pages.
* `/index.html`: The main entrypoint of the application which is served from every valid route. This file should be very small, since it will be served from different URLs therefore be cached multiple times. All resource URLs in the entrypoint need to be absolute, since it may be served from non-top-level URLs
* `/polymer.json`: This file allows you to store information about your project structure and desired build configuration(s). It is used by the Polymer CLI as a way to understand the structure of your application.


```nohighlight
|---/src
|   |___/components
|   |   |____/....
|   |   |____/....
|   |   |____/....
|   |
|   |___/pages
|   |   |____/products.html (component definition name: products-page)
|   |   |____/products-accounts.hml (component definition name: products-accounts-page)
|   |
|   |___/app-shell.html
|   |___/app.js
|
|___/....
|___/polymer.json
|___/index.html
```

### Shell

An application shell is the minimal HTML, CSS, and JavaScript powering a user interface. The application shell should:

load fast
be cached
dynamically display content
An application shell is the secret to reliably good performance. Think of your app's shell like the bundle of code you'd publish to an app store if you were building a native app. It's the load needed to get off the ground, but might not be the whole story. It keeps your UI local and pulls in content dynamically through an API.

![shellExample](https://developers.google.com/web/updates/images/2015/11/appshell/appshell-1.jpg)

You can define your shell in the polymer.json file in your project directory.

```javascript
{
  "entrypoint": "index.html",
  "shell": "src/app-shell.html"
  ...
}
```
Contains the main code that boots the app and loads necessary resources. The shell usually includes the common UI for the app, and the router, which loads resources needed for the current route.

You can read more about Shell pattern [here](https://developers.google.com/web/updates/2015/11/app-shell)

### Pages

TBD.

### Components

TBD.

**[⬆️ back to top](#appendix)**

## <a name="sample-app"></a>6. Sample app

[cells-static-sample-app] showcases the use and features of Cells Static library in combination with Polymer CLI tooling to build modular, maintainable, & high scalable Progressive Web Apps based on Polymer 2.

[cells-static-sample-app]: https://globaldevtools.bbva.com/bitbucket/projects/CELLSLABS/repos/cells-static-sample-app/browse?at=refs%2Fheads%2Fdevelop

**[⬆️ back to top](#appendix)**
